#include "CMachineInfo.h"
#include <QNetworkSession>
#include <QSysInfo>
#include <QNetworkAccessManager>
#include <QtMqtt/QMqttClient>


MachineInfo::MachineInfo(QObject *parent) : QObject(parent), m_IPAddress("127.0.0.1"), m_HasInternet(false), m_networkManager(nullptr)
{
    connect(&m_timer, &QTimer::timeout, this, &MachineInfo::timerTick);
    m_timer.start(5000);
}


QString MachineInfo::ipAddress()
{
    return m_IPAddress;
}


void MachineInfo::setIpAdress(const QString &ipAddress)
{
    if(m_IPAddress != ipAddress)
    {
        m_IPAddress = ipAddress;
        emit ipAddressChanged();
    }
}

QString MachineInfo::cpuArch()
{
    return QSysInfo::currentCpuArchitecture();
}

QString MachineInfo::hasInternetConnection()
{
    QString result = "No Internet";
    if(m_HasInternet)
    {
        result = "Internet Reachable";
    }
    return result;
}


void MachineInfo::setHasInternetConnection(bool hasConnection)
{
    if(hasConnection != m_HasInternet)
    {
        m_HasInternet = hasConnection;
        emit hasInternetConnectionChanged();
    }
}

void MachineInfo::networkCheckFinished(QNetworkReply *reply)
{
    if(reply->error() == QNetworkReply::NoError)
    {
        setHasInternetConnection(true);
   }
    reply->close();
    reply->deleteLater();
}

void MachineInfo::timerTick()
{
    qDebug() << "TICK";
    foreach (const QNetworkInterface &netInterface, QNetworkInterface::allInterfaces()) {
        QNetworkInterface::InterfaceFlags flags = netInterface.flags();
        if( (bool)(flags & QNetworkInterface::IsRunning) && !(bool)(flags & QNetworkInterface::IsLoopBack)){
            foreach (const QNetworkAddressEntry &address, netInterface.addressEntries()) {
                if(address.ip().protocol() == QAbstractSocket::IPv4Protocol)
                {
                    qDebug() << address.ip().toString();
                    QString tmp = address.ip().toString();
                    setIpAdress(tmp);
                }
            }
        }
    }


    if(m_networkManager == nullptr)
    {
        m_networkManager = new QNetworkAccessManager(this);
        connect(m_networkManager, &QNetworkAccessManager::finished,
                this, &MachineInfo::networkCheckFinished);

    }
    m_networkManager->get(QNetworkRequest(QUrl("http://qt-project.org")));
}
