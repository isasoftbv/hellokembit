#ifndef CMACHINEINFO_H
#define CMACHINEINFO_H

#include <QObject>
#include <QtGui>
#include <qqml.h>
#include <QNetworkReply>
#include <QTimer>
#include <QNetworkAccessManager>


class MachineInfo : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString ipAddress READ ipAddress WRITE setIpAdress NOTIFY ipAddressChanged)
    Q_PROPERTY(QString cpuArch READ cpuArch)
    Q_PROPERTY(QString hasInternetConnection READ hasInternetConnection NOTIFY hasInternetConnectionChanged)
    QML_ELEMENT

public:
    explicit MachineInfo(QObject *parent = nullptr);

    QString ipAddress();
    void setIpAdress(const QString& ipAddress);

    QString cpuArch();

    QString hasInternetConnection();
    void setHasInternetConnection(bool hasConnection);

private:
    QString m_IPAddress;
    bool m_HasInternet;
    QTimer m_timer;
    QNetworkAccessManager* m_networkManager;


signals:
    void ipAddressChanged();
    void hasInternetConnectionChanged();

public slots:
    void networkCheckFinished(QNetworkReply* reply);
    void timerTick();


};

#endif // CMACHINEINFO_H
