import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.VirtualKeyboard 2.15
import QtQuick.Layouts 1.0
import org.isasoft.armstrong.machineinfo 1.0
import QtQuick.Controls 2.15

Window {
    id: window
    width: 800
    height: 480
    visible: true
    color: "#2c2427"
    title: qsTr("Project Armstrong")

    MachineInfo {
        id: machineInfo
    }

    InputPanel {
        id: inputPanel
        z: 99
        x: 0
        y: window.height
        width: window.width

        states: State {
            name: "visible"
            when: inputPanel.active
            PropertyChanges {
                target: inputPanel
                y: window.height - inputPanel.height
            }
        }
        transitions: Transition {
            from: ""
            to: "visible"
            reversible: true
            ParallelAnimation {
                NumberAnimation {
                    properties: "y"
                    duration: 250
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }

    Text {
        id: text1
        color: "#ffffff"
        text: qsTr("KEMBIT")
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        font.pixelSize: 25
        horizontalAlignment: Text.AlignHCenter
        anchors.bottomMargin: 407
        anchors.rightMargin: 162
        anchors.leftMargin: 546
        anchors.topMargin: 35
        minimumPixelSize: 28
    }

    Text {
        id: text2
        color: "#ffffff"
        text: qsTr("Project Armstrong")
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        font.pixelSize: 25
        horizontalAlignment: Text.AlignHCenter
        anchors.bottomMargin: 353
        anchors.rightMargin: 95
        anchors.topMargin: 89
        minimumPixelSize: 28
        anchors.leftMargin: 479
    }

    Image {
        id: image
        x: 23
        y: 20
        width: 268
        height: 156
        source: "kembitlogo.svg"
        fillMode: Image.PreserveAspectFit
    }

    Text {
        id: text3
        color: "#ffffff"
        text: qsTr("Development Target")
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        font.pixelSize: 25
        horizontalAlignment: Text.AlignHCenter
        anchors.bottomMargin: 291
        anchors.rightMargin: 8
        anchors.topMargin: 151
        minimumPixelSize: 28
        anchors.leftMargin: 392
    }

    Grid {
        id: grid
        x: 0
        y: parent.height - (parent.height/2)
        width: parent.width
        height: parent.height/2
        leftPadding: 20
        topPadding: 20
        rows: 3
        columns: 2
        spacing: 25

        Text {
            id: ipLabel
            color: "#ffffff"
            horizontalAlignment: Text.AlignLeft
            text: qsTr("Ip Adress")
        }
        Text {
            id: ipValue
            color: "#ffffff"
            horizontalAlignment: Text.AlignLeft
            text: machineInfo.ipAddress
        }
        Text {
            id: hasInternetLabel
            color: "#ffffff"
            horizontalAlignment: Text.AlignLeft
            text: qsTr("Connected to Internet")
        }
        Text {
            id: hasInternetValue
            color: "#ffffff"
            horizontalAlignment: Text.AlignLeft
            text: machineInfo.hasInternetConnection
        }
        Text {
            id: cpuArchLabel
            color: "#ffffff"
            horizontalAlignment: Text.AlignLeft
            text: qsTr("Cpu Architecture")
        }
        Text {
            id: cpuArchValue
            color: "#ffffff"
            horizontalAlignment: Text.AlignLeft
            text: machineInfo.cpuArch
        }
    }

}
