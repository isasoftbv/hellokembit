#include "mqttdispatcher.h"

MQTTDispatcher &MQTTDispatcher::instance()
{
    static MQTTDispatcher* _instance = nullptr;

    if(_instance == nullptr)
    {
        _instance = new MQTTDispatcher(nullptr);
    }
    return *_instance;

}

MQTTDispatcher::MQTTDispatcher(QObject *parent) : QObject(parent)
{

}
