#ifndef MQTTDISPATCHER_H
#define MQTTDISPATCHER_H

#include <QObject>
#include <QtMqtt>

class MQTTDispatcher : public QObject
{
    Q_OBJECT
public:

    static MQTTDispatcher& instance();

private:
    explicit MQTTDispatcher(QObject *parent = nullptr);



signals:
};

#endif // MQTTDISPATCHER_H
